<?php

require $_SERVER['DOCUMENT_ROOT'].'/bugreport/class.phpmailer.php';

$to		= 'info@one-pdf.com';
$from	= 'contact@one-pdf.com';
$name    = stripslashes($_POST["name"]);
$email   = stripslashes($_POST["email"]);
$message = nl2br(stripslashes(htmlspecialchars($_POST["message"])));

function fin($msg) {
	http_response_code(422);
	die($msg);
}

function logSpamIP($reason = "no reason given") {
    $open = fopen($_SERVER['DOCUMENT_ROOT']."/../includes/spamips.txt", "a+");
    if ($open) {
		fwrite($open, $_SERVER['REMOTE_ADDR']." - ".date("D M dS H:i:s")." - " . $reason . "\n");
		fclose($open);
    }
    fin('Your email was not sent<br>due to malicious content ('.$reason.')');
}

foreach(array($name, $email) as $value) {
    if (preg_filter("/(From:|To:|BCC:|CC:|Subject:|Content-Type:)/i", "", $value) !== NULL) {
		logSpamIP("header injection");
    }
}

// Basic keyword spam filter
$terms = array();
if (preg_match("/(casino|lottery|viagra|sex|penis|pharmacy|shipping!|online!)/i", $message, $terms)) {
	logSpamIP("common spam term" . (isset($terms[0]) ? " '" . $terms[0] . "'" : ""));
}

if ($name == '' || $email == '' || $message == '') {
    fin('Please fill in all the information');
}

if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    fin('Invalid Email Address');
}

if ((strpos($name, '\n')  !== FALSE) || (strpos($name,'\r')  !== FALSE) ||
	(strpos($email, '\n') !== FALSE) || (strpos($email,'\r') !== FALSE)) {
    logSpamIP("unexpected newline");
}

try {
	$mail 				= new PHPMailer();
	$mail->CharSet		= "UTF-8";
	$mail->AddAddress($to);
	$mail->AddReplyTo($email);
	$mail->SetFrom($from);
	$mail->Subject  	= "Message from $name";
	$mail->Body    		= $message;
	$mail->IsHTML(true);
	$mail->Send();
} catch (phpmailerException $e) {
	$str = $e->errorMessage()."\r\n\r\n".var_export($_POST, true);
	file_put_contents("./dump".md5($str).".txt", $str);
	fin("Something went wrong.");
} catch (Exception $e) {
	$str = $e->getMessage()."\r\n\r\n".var_export($_POST, true);
	file_put_contents("./dump".md5($str).".txt", $str);
	fin("Something went wrong.");
}
echo "Message sent!";
?>