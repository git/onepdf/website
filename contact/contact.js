document.forms[0].addEventListener('submit', function(e) {
	var msg = document.getElementById("msg");
	msg.innerHTML = '';
	e.preventDefault();
	xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == XMLHttpRequest.DONE) {
			if (xmlhttp.status != 200) msg.className = 'error';
			else msg.className = '';
			msg.innerHTML = xmlhttp.responseText;
			
			if (xmlhttp.status == 200) _paq.push(['trackEvent', 'Contact', 'Submit Form', 'onepdf-contact-form']);
		}
	}
	
	xmlhttp.open("POST", "submit.php", true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send(serialize(document.forms[0]));
});