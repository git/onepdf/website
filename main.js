/**
 * No jQuery or anything! Yay!
 * Wait, that just means I basically wasted time recoding
 * things other people already did, right?
 *
 * ...
 * 
 * I don't care. IN YOUR FACE, LIBRARIES!
 */

/**
 * Stuff to be done ASAP
 */
(function(){
	document.documentElement.className = document.documentElement.className.replace('nojs','');
	window.requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.oRequestAnimationFrame;
})();

/**
 * Easing curve definitions
 */
var Easing = {
	linear: function (t) { return t },
	easeInCubic: function (t) { return t*t*t },
	easeOutCubic: function (t) { return (--t)*t*t+1 },
	easeInOutCubic: function (t) { return t<.5 ? 4*t*t*t : (t-1)*(2*t-2)*(2*t-2)+1 },
	easeOutElastic: function (t) {
		var p = 0.3;
		return Math.pow(2,-15*t) * Math.sin((t-p/4)*(2*Math.PI)/p) + 1;
	}
};

/**
 * Element Fading with a CSS class. Not ideal, but causes less
 * glitchiness when the page has been out of view.
 * e = element
 */
function cssToggleFade(e) {
	if (e.className.indexOf("fadein") > -1) {
		e.className = e.className.replace('fadein','');
	} else {
		e.className += 'fadein';
	}
}

/**
 * Basic slideshow initiation
 * slides = array of IDs for slides, fading = true if fade animations
 * returns function to start slideshow
 */
function slideShowInit(slides, fading) {
	var idx = 0;
	return function(timeout, speed) {
		var slidefunc = function() {
			var sl = slides.length;
			for (var i = 0; i < sl; i++) {
				var e = document.getElementById(slides[i]);
				if ((i == idx && e.className.indexOf("fadein") < 0) ||
					(i != idx && e.className.indexOf("fadein") > -1)) {
					cssToggleFade(e);
				}
			}
			idx = (idx + 1) % sl;
		};
		slidefunc();
		setInterval(function(){
			slidefunc();
		}, timeout);
	};
}
/**
 * Animation and fade scheduling
 */
window.addEventListener('load', function () {
	var slideShow = slideShowInit(["slide1", "slide2", "slide3"], true);
	
	var images = {
	    png:		{img: new Image, loaded: false},
	    txt:		{img: new Image, loaded: false},
	    folder:		{img: new Image, loaded: false},
	    pdf:		{img: new Image, loaded: false},
	    logoback:	{img: new Image, loaded: false},
	    logofront:	{img: new Image, loaded: false},
	    //shadow:		{img: new Image, loaded: false},
	};
	var canvas, ctx, scale = window.devicePixelRatio;
	canvas = document.getElementById('merge');
	canvas.width =  400;
	canvas.height = 617;
	canvas.style.width =  400 + "px";
	canvas.style.height = 617 + "px";
	
	canvas.setAttribute('width',  400 * scale);
	canvas.setAttribute('height', 617 * scale);
	
    	ctx = canvas.getContext('2d');
	ctx.scale(scale, scale);
  
	images.folder.img.src = 'img/icon/folder@2x.png';
	images.txt.img.src = 'img/icon/txt@2x.png';
	images.png.img.src = 'img/icon/png@2x.png';
	images.pdf.img.src = 'img/icon/pdf@2x.png';
	images.logoback.img.src = 'img/logo/back@2x.png';
	images.logofront.img.src = 'img/logo/front@2x.png';
	
	var nextValue = function(start, end, curr, tot, ease) {
		if (!ease) ease = Easing.easeInOutCubic;
		return (end - start) * ease(curr / tot) + start;
	}
	var nextCoords = function(source, target, curr, total, ease) {
		// source and target had better contain the same keys...
		// but screw checking it, I don't need this to be any more inefficient than it is now.
		var ans = {};
		for (var key in source) {
			ans[key] = nextValue(source[key], target[key], curr, total, ease);
		}
		return ans;
	}
	var startAnimation = function() {
		var frame = 0;
		var anim = setInterval(function() {
			frame++;
			ctx.globalAlpha = Math.min(1.0, nextValue(0.0, 1.0, frame, 40, Easing.easeInOutCubic));
			
			ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
			ctx.drawImage(images["logoback"].img, 80, 210, 240, 43);
			if (frame > 0 && frame <= 40) {
				// Initial Drawing
				
				ctx.shadowColor = "rgba(0, 0, 0, 0.6)"; ctx.shadowBlur = 5;
				
				ctx.drawImage(images["png"].img, 50, 67, 56, 76);
				ctx.drawImage(images["folder"].img, 162, 67, 76, 76);
				ctx.drawImage(images["txt"].img, 294, 67, 56, 76);
				
				ctx.shadowBlur = 0; ctx.shadowColor = "rgba(0, 0, 0, 0)";
			}
			if (frame > 40 && frame <= 100) {
				// Files Into Funnel
				
				ctx.shadowColor = "rgba(0, 0, 0, 0.6)"; ctx.shadowBlur = 5;
				
				var target		= {x: 186,	y: 264, w: 28, h: 38}
				,   targetfol	= {x: 181,	y: 264, w: 38, h: 38}
				,   sourcepng	= {x: 50, 	y: 67,  w: 56, h: 76}				
				,   sourcefol	= {x: 162,	y: 67,  w: 76, h: 76}				
				,   sourcetxt	= {x: 294,	y: 67,  w: 56, h: 76};
				var steppng = nextCoords(sourcepng, target,		frame - 40, 60);
				var stepfol = nextCoords(sourcefol, targetfol,	frame - 40, 60);
				var steptxt = nextCoords(sourcetxt, target,		frame - 40, 60);
				ctx.drawImage(images["png"].img, steppng.x, steppng.y, steppng.w, steppng.h);
				ctx.drawImage(images["txt"].img, steptxt.x, steptxt.y, steptxt.w, steptxt.h);
				ctx.drawImage(images["folder"].img, stepfol.x, stepfol.y, stepfol.w, stepfol.h);
				
				ctx.shadowBlur = 0; ctx.shadowColor = "rgba(0, 0, 0, 0)";
			}
			if (frame > 90 && frame <= 110) {
				// PDF partly out of funnel
				
				var target = {x: 189, y: 422, w: 20, h: 27}
				,   source = {x: 189, y: 408, w: 20, h: 27};
				var step = nextCoords(source, target, frame - 90, 20, Easing.easeInCubic);
				ctx.drawImage(images["pdf"].img, step.x, step.y, step.w, step.h);
			}
			if (frame > 110) {
				// PDF elastic out of funnel
				
				ctx.shadowColor = "rgba(0, 0, 0, 0.6)"; ctx.shadowBlur = 5;
				
				var target = {x: 172, y: 494, w: 56, h: 76}
				,   source = {x: 189, y: 422, w: 20, h: 27};
				var step = nextCoords(source, target, frame - 110, 60, Easing.easeOutElastic);
				ctx.drawImage(images["pdf"].img, step.x, step.y, step.w, step.h);
				
				ctx.shadowBlur = 0; ctx.shadowColor = "rgba(0, 0, 0, 0)";
			}
			ctx.drawImage(images["logofront"].img, 80, 210 + 22, 240, 212);
			// Kick off fades and slideshows
			if (frame == 160) {
				// "Say Hello" header
				cssToggleFade(document.getElementById("sayhello"));
			}
			if (frame == 200) {
				// Slideshow
				slideShow(10000, 800);
				clearInterval(anim);
			}
		}, 1000/40);
	}
	
	// Begins animation once images are loaded
	var loaded = 0;
	for (var key in images) {
		images[key].img.addEventListener('load', function () {
			images[key].loaded = true;
			loaded++;
			if (loaded == Object.keys(images).length) {
				startAnimation();
			}
		}, false);
	}
	
}, false);